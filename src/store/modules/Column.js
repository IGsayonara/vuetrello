import Vue from 'vue'

export default {
    actions: {
        async fetchColumns(ctx){
            const res = await fetch(
                "https://jsonplaceholder.typicode.com/posts?_limit=3"
            );
            const columns = await res.json();
            ctx.commit('updateColumns', columns);
        },
        addColumn: function(ctx, id){
            ctx.commit('addColumn', id);
        },
        removeColumn: function(ctx, id){
            ctx.commit('deleteColumn', id);
        }
    },
    mutations: {
        updateColumns(state, columns){
           state.columns = columns; 
        },
        addColumn(state, id){
            state.columns.push({title: 'new Col', id: id})
        },
        deleteColumn(state, id){
            state.columns.forEach((value, index) => {
                if(value.id === id){
                    state.columns.splice(index, 1);
                }
            })
        }
    },
    state: {
        columns: [],
    },
    getters: {
        allColumns(state){
            return state.columns;
        }
    },
}