import Vue from 'vue'

export default {
    actions: {
        async fetchCarts(ctx){
            const res = await fetch(
                "https://jsonplaceholder.typicode.com/posts?_limit=5"
            );
            const carts = await res.json();
            ctx.commit('updateCarts', carts);
        },
        removeCart(ctx, id){
            ctx.commit('deleteCart', id);
        },
        addCart(ctx, data){
            ctx.commit('addCart', data);
        },
        cartToEdit(ctx, id){
            ctx.commit('cartToEdit', id);
        },
        cartFilled(ctx, id){
            ctx.commit('cartFilled', id)
        }
    },
    mutations: {
        updateCarts(state, carts){
           state.carts = carts;
           let counter = 0; 
           state.carts.forEach(element => {
               element.columnId = counter%2 + 1;
               counter++;
               element.filled = true;
           });
        },
        deleteCart(state, id){
            state.carts.forEach((value, index) => {
                if(value.id === id){
                    state.carts.splice(index, 1);
                }
            })
        },
        addCart(state, data){
            state.carts.push({
                id: data.id,
                title: 'title',
                body: 'body',
                columnId: data.col,
                filled: false
            })
        },
        cartToEdit(state, id){
            state.carts.forEach((el, index) => {
                if(el.id === id){
                    el.filled = false
                    let newEl = el;
                    Vue.set(state.carts, index, newEl)
                }
            })
        },
        cartFilled(state, id){
            state.carts.forEach((el, index) => {
                if(el.id === id){
                    console.log(el)
                    el.filled = true
                    let newEl = el;
                    Vue.set(state.carts, index, newEl)
                }
            })
        }
    },
    state: {
        carts: [],
    },
    getters: {
        allCarts(state){
            return state.carts;
        },
        sortedCarts: (state) => (id) => {
            let sorted = new Array();
            state.carts.forEach(el => {
                if(el.columnId === id){
                    sorted.push(el)
                }
            })
            return sorted;
        }
    },
}