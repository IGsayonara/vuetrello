import Vue from 'vue'
import Vuex from 'vuex'
import column from './modules/Column'
import cart from './modules/Cart'
Vue.use(Vuex)



export default new Vuex.Store({
    modules: {
        column,
        cart
    }
})