import Vue from 'vue'
import { Vue2Dragula } from 'vue2-dragula'
import App from './App.vue'
import Vuex from 'vuex'
import store from './store'

Vue.use(Vuex)

Vue.use(Vue2Dragula, {
  logging: {
    service: true // to only log methods in service (DragulaService)
  }
});

import 'bootstrap/dist/css/bootstrap.css'
import 'dragula/dist/dragula.css'



Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
